package com.task10_Annotation.model;

import com.task10_Annotation.model.MyAnnotation;
import java.util.Objects;

public class MyClass {
  @MyAnnotation(info = "Field name is 'count'")
  private int count;
  //@MyAnnotation
  private String name;
  @MyAnnotation
  private static boolean isNecessary;
  private final String alphabet = "abcdefghjklmnopqrstuvwyxz";

  public void printThisClass(){
    System.out.println(this.getClass());
  }

  public String randomString(int numOfLetters){
    StringBuilder builder = new StringBuilder();
    while (numOfLetters-- != 0){
      int character = (int)(Math.random()*alphabet.length());
      builder.append(alphabet.charAt(character));
    }
    String randomString = builder.toString();
    return randomString;
  }

  public void myMethod(String a, int...args){
    System.out.println(a + " " + args.length);
  }

  public void myMethod(String...args){
    for (String arg : args) {
      System.out.println(arg);
    }
  }

  public boolean isDone(String st, Integer integer){
    int num = integer;
    return (st.length() == num);
  }

  public MyClass() {
  }

  public MyClass(int count, String name) {
    this.count = count;
    this.name = name;
  }

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public static boolean isIsNecessary() {
    return isNecessary;
  }

  public static void setIsNecessary(boolean isNecessary) {
    MyClass.isNecessary = isNecessary;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MyClass myClass = (MyClass) o;
    return count == myClass.count &&
        Objects.equals(name, myClass.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(count, name);
  }

  @Override
  public String toString() {
    return "MyClass{" +
        "count=" + count +
        ", name='" + name + '\'' +
        '}';
  }
}
