package com.task10_Annotation.model;

public class Receiver {
  public void showInfo(Object object){
    Class<?> objectClass = object.getClass();
    Package aPackage = objectClass.getPackage();
    System.out.println(aPackage);
  }
}
