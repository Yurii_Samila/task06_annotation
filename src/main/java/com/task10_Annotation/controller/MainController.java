package com.task10_Annotation.controller;

import com.task10_Annotation.model.MyAnnotation;
import com.task10_Annotation.model.MyClass;
import com.task10_Annotation.model.Receiver;
import com.task10_Annotation.view.View;
import java.lang.reflect.AnnotatedType;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class MainController {

  public static void main(String[] args)
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
    View view = new View();
    Receiver receiver = new Receiver();
    Class<MyClass> myClazz = MyClass.class;
    //--------------------Task2------------------------
    Field[] fields = myClazz.getDeclaredFields();
    for (Field field : fields) {
      if (field.isAnnotationPresent(MyAnnotation.class)){
        view.print(field.getName());
    //--------------------Task3---------------------------
        view.print(field.getAnnotation(MyAnnotation.class).info());
      }
    }
    //-------------------Task4-----------------------------
    MyClass myClass = myClazz.getDeclaredConstructor().newInstance();
    Method randomString = myClazz.getMethod("randomString", int.class);
    String st = (String) randomString.invoke(myClass, 8);
    view.print(st);
    Method[] methods = myClazz.getDeclaredMethods();
    for (Method method : methods) {
      Class<?> returnType = method.getReturnType();
      if (returnType.equals(boolean.class)){
        view.print("This is method with boolean return type: " + method.getName());
      }
    }
    //---------------------Task5-----------------------------
    Field[] declaredFields = myClazz.getDeclaredFields();
    for (Field declaredField : declaredFields) {
      Class<?> type = declaredField.getType();
      declaredField.setAccessible(true);
      if (type.equals(String.class)){
        declaredField.set(myClass, "SETTED");
        view.print("Field " + declaredField.getName() + " has value: " + declaredField.get(myClass));
      }
    }
    //----------------------Task6----------------------------
    Method[] declaredMethods = myClazz.getDeclaredMethods();
    for (Method declaredMethod : declaredMethods) {
      if (declaredMethod.getName().equals("myMethod")){
        if (declaredMethod.getParameterCount() == 1){
          String [] strings = {"Car", "Plane", "Train", "Bus"};
          String[][] newStrings = {strings};
          declaredMethod.invoke(myClass, newStrings);
        }else {
          int[]ints = new int[]{2,5,6};
          declaredMethod.invoke(myClass, "Java", ints);
        }
      }
    }
//    ---------------------------Task7-----------------------
    receiver.showInfo(myClass);
  }
  }

